<!-- START doctoc generated TOC please keep comment here to allow auto update -->


**Table of Contents**

- [Intro](#markdown-header-intro)
- [MUST-DO rules](#markdown-header-must-do-rules)
- [Coding suggestions](#markdown-header-coding-suggestions)
    - [Header ifdef for Windows/Linux/Mac](#markdown-header-header-ifdef-for-windowslinuxmac)
    - [Waiting for user to press <enter>](#markdown-header-waiting-for-user-to-press-enter)
        - [C++](#markdown-header-c)
        - [C](#markdown-header-c)
    - [Measuring execution time](#markdown-header-measuring-execution-time)
        - [C++](#markdown-header-c_1)
        - [C](#markdown-header-c_1)
    - [Sleeping for 50 milliseconds in the current thread](#markdown-header-sleeping-for-50-milliseconds-in-the-current-thread)
        - [C++](#markdown-header-c_2)
        - [C](#markdown-header-c_2)
    - [Reading a numeric input from a user](#markdown-header-reading-a-numeric-input-from-a-user)
        - [C++](#markdown-header-c_3)
        - [C](#markdown-header-c_3)
- [Coding suggestions: PTHREADS](#markdown-header-coding-suggestions-pthreads)
    - [Semaphores](#markdown-header-semaphores)
        - [Mac OSX](#markdown-header-mac-osx)
    - [Barriers](#markdown-header-barriers)
        - [Mac OSX](#markdown-header-mac-osx_1)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->



# Intro

We provide here a generic set of rules and coding conventions for various 
programming courses for students at HiG. Specifically, the guide is aimed 
at Game Programming students. The source code should follow the conventions laid out in [C++ Style Guide](https://bitbucket.org/gtl-hig/coding-conventions/src/048a94b6983528c084ab3459eb226d91c6a88534/cpp_style_guide.md?at=master)


# MUST-DO rules

* `.c` file has to be a C-language source code, and comply with C99 standard. 
* `.cpp` file has to be a C++ language source code, and comply with C++11 standard.
* Make an effort to write portable code. *DO NOT USE* platform specific extensions unless those are required to achive desired functionality on a single specific architecture. 



# Coding suggestions

As a general rule, try to avoid platform specific code if a generic portable solution is available. Here are some typical examples.


## Header ifdef for Windows/Linux/Mac

For compilation that require variation between Windows, Linux and MacOSX, you can use one of the defined constants. An example pattern is provided below:

```c++
#ifdef _WINDOWS
    //  Windows specific things

#elseif __APPLE__
    // MacOSX specific things 

#else
    // Standard Unix/Linux things

#endif
```


## Waiting for user to press <enter>

### C++

```c++
 	std::cout << "Press <enter> to continue..." << std::endl;
	std::cin.clear();
	std::cin.ignore();
``` 

*Note, do not use:*

```c++
	system("pause");
```

### C

```c
	#include <stdio.h>
	...
	
	printf("Press <enter> to continue...");
	getchar();
``` 

## Measuring execution time

### C++

```c++
#include <chrono>
...

auto startTime = chrono::high_resolution_clock::now();
...

auto durationAbsolute = chrono::high_resolution_clock::now() - startTime;
auto msDuration = chrono::duration_cast<chrono::milliseconds>(durationAbsolute).count();
```

### C

There are two basic constructs inside `<time.h>`: `clock()` and `time()`. The former one is more accurate, as it actually counts the ticks of the CPU. However, you have to adjust for the fact that on the multicore system it will report cumulative ticks from all of your cores, not the wall clock.

```c
#include <time.h>
...

clock_t start, end;
start = clock();
...

stop = clock();
double nanoDiff = end - start;
double msDiff = ((double)(nanoDiff) / CLOCKS_PER_SEC) * 1000;
```

If you do not need nano-seconds accuracy, you might use time() instead.

```
#include <time.h>
...

time_t start, end;
double secondsDiff;


time(&start);
...

time(&stop);
secondsDiff = difftime(end, start);
```





## Sleeping for 50 milliseconds in the current thread

### C++

```
#include <chrono>
#include <thread>
...

std::this_thread::sleep_for(std::chrono::milliseconds(50));
```

### C

`Sleep(milliseconds)` is not available on non-Windows systems, and `usleep(microseconds)` is not available on Windows. Below is one of the possible ways solving this to provide portability:

```
#ifdef _WINDOWS
#include <windows.h>
#else
#include <unistd.h>
#define Sleep(x) usleep((x)*1000)
#endif
```


## Reading a numeric input from a user

### C++ 

```
#include <iostream>
#include <string>
...

std::string input;
getline(std::cin, input);
int myint = std::stoi(input);
```


### C

```
char line[256];
int num;
if (fgets(line, sizeof(line), stdin)) {
    if (1 == sscanf(line, "%d", &num)) {
        // num - contains the entered numerical value
    }
}
```


# Coding suggestions: PTHREADS

Unfortunately, not all elements of POSIX threads are available on all platforms. Sometimes a bit of trickery is needed. Below are some useful suggestions.


## Semaphores

### Mac OSX

To make use of semaphores on MacOSX, you have to use named semaphores, instead of the unnamed ones. This means that `sem_init` will not work, but instead, you have to use: `sem_open`, `sem_close`, `sem_ulink`. 

```
    sem_t *s;

    if ((s = sem_open("/sem", O_CREAT, 0644, 1)) == SEM_FAILED ) {
        perror("sem_open");
        exit(EXIT_FAILURE);
    }

    // Here you have the semaphore initialized
    // e.g. use: sem_wait(s) and sem_post(s);

    if (sem_close(s) == -1) {
        perror("sem_close");
        exit(EXIT_FAILURE);
    }

    if (sem_unlink("/sem") == -1) {
        perror("sem_unlink");
        exit(EXIT_FAILURE);
    }

```


## Barriers

### Mac OSX

To make your POSIX pthreads code use barriers and run correctly on MacOSX, you need to include the following definitions. *Note*, this is a good example of implementing barrier with mutex and conditional variables, that can be used to implement barriers in C++11.

```
#ifdef __APPLE__

#ifndef PTHREAD_BARRIER_H_
#define PTHREAD_BARRIER_H_

#include <pthread.h>
#include <errno.h>

typedef int pthread_barrierattr_t;
typedef struct {
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    int count;
    int tripCount;
} pthread_barrier_t;


int pthread_barrier_init(pthread_barrier_t *barrier, const pthread_barrierattr_t *attr, unsigned int count) {

    if (count == 0) {
        errno = EINVAL;
        return -1;
    }

    if (pthread_mutex_init(&barrier->mutex, 0) < 0) {
        return -1;
    }

    if (pthread_cond_init(&barrier->cond, 0) < 0) {
        pthread_mutex_destroy(&barrier->mutex);
        return -1;
    }

    barrier->tripCount = count;
    barrier->count = 0;

    return 0;
}

int pthread_barrier_destroy(pthread_barrier_t *barrier) {
    pthread_cond_destroy(&barrier->cond);
    pthread_mutex_destroy(&barrier->mutex);
    return 0;
}

int pthread_barrier_wait(pthread_barrier_t *barrier) {
    pthread_mutex_lock(&barrier->mutex);
    ++(barrier->count);
    if (barrier->count >= barrier->tripCount) {
        barrier->count = 0;
        pthread_cond_broadcast(&barrier->cond);
        pthread_mutex_unlock(&barrier->mutex);
        return 1;
    } else {
        pthread_cond_wait(&barrier->cond, &(barrier->mutex));
        pthread_mutex_unlock(&barrier->mutex);
        return 0;
    }
}

#endif // PTHREAD_BARRIER_H_
#endif // __APPLE__
```

